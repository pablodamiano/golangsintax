package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func DBConn() (db *sql.DB) {
	//connect data base
	db, err := sql.Open("mysql", "golang:4X+9zXs3k6%1@tcp(143.198.239.58:3306)/golang")
	if err != nil {
		panic(err.Error())
	}

	//db.Query("INSERT INTO 'sms_message_regis' ('id','number', 'message', 'origen') VALUES ('12323434', 'Farley','Mexico');")
	return db
}

/*
	//var error = db.Ping()
	fmt.Println("--------  DB PING -----------")
	fmt.Println(error)
	fmt.Println("----------DB PING---------")
	//defer db.Close()
	fmt.Println("Successfully connect server")
*/
