package Models

import "gorm.io/gorm"

type Food struct {
	gorm.Model
	Type  string
	Price string
	Call  string
}
