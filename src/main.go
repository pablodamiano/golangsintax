package main

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"gorm.io/driver/mysql"

	"gorm.io/gorm"

	"servidor/src/Controllers"
)

var dbgorm *gorm.DB
var errgorm error

func setupRouter() *gin.Engine {
	dsn := "golang:4X+9zXs3k6%1@tcp(143.198.239.58:3306)/golang?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	dbgorm = db
	errgorm = err
	fmt.Println(db, err)

	r := gin.Default()

	r.POST("/api", smscontroller.RegisterSmS)

	r.GET("/get-urls", smscontroller.GetURls)

	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
		"foo":  "bar", // user:foo password:bar
		"manu": "123", // user:manu password:123
	}))

	authorized.POST("admin", func(c *gin.Context) {
	})

	return r
}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":8080")
}
